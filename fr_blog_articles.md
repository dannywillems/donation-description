## Contact, politique de contenu et dons

Vous pouvez me contacter à l'adresse email be.danny.willems@gmail.com. N'hésitez
pas à soumettre des idées d'articles.

Ce site est et sera **toujours** gratuit, sans publicité, respecte et
respectera la vie privée. Aucune donnée n'est
sauvegardée, et les sources des articles sont et seront toujours accessibles
librement. Le savoir est un droit, et doit être libre d'accès.

Cependant, écrire des articles de qualité prend du temps et nécessite un
apprentissage, une recherche et de l'expérimentation. La rédaction d'un article
peut prendre de plusieurs heures à plusieurs jours voire plusieurs semaines.

Si vous trouvez que le contenu de l'article est de bonne qualité et utile, vous
pouvez effectuer un don. Si en plus l'article vous a permis de gagner de
l'argent, pourquoi ne pas remercier l'auteur en versant un petit pourcentage
du montant économisé ?

Actuellement, uniquement les crypto monnaies sont supportées. Voici différentes
adresses réservées aux dons, la liste n'étant pas exhaustive (un simple email
pour générer une nouvelle adresse est cependant nécessaire):

Crypto monnaies:
- BAT : visitez [mon compte GitHub avec Brave](https://github.com/dannywillems)
- Bitcoin : 3K4MqbXt6ntdzw6Bbiqqu7hkkMMiQAMUVT
- Bitcoin Cash : 14SjdJjHhSCJjxSBm2a77HiMLgeDLz39rP
- Ethereum (et USDT ou tout jeton ERC20) : 0x1Fa2Fef13E46EF709cd1af8ab243d7cB206fA3c4
- Dash : Xu8NZtBc9re9mhoxgL8Z4RA2TwjeCgJQCN
- XRP (Ripple) : rD1Jj7VJACyUT6f5YuDWGPSCq4SkKBaPsp
- XTZ (Tezos) : tz1XHurCqVCp5YFCqS8GgFhFAhzSccaBgxh6
- ZCash : t1RD64KeERzsDD2oxkB18aiXZnKnVf6vN8v
